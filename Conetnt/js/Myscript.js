
$('.Topcarousel').owlCarousel({
    loop: true,
    // margin: 30, 
    dots: true,
    autoplay: false,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    nav: false,
    // navText: ["<div class='btn1'> <i class='glyphicon glyphicon-arrow-left navleft' ></i></div>",
    //     "<div class='btn2'><i class='glyphicon glyphicon-arrow-right navright'></i></div>"],
    responsive: {
        0: {
            items: 1
        },
        735: {
            items: 1
        },
        1000: {
            items: 1
        }
    }

})


$('.productcarousel').owlCarousel({
    loop: true,
    // margin: 30, 
    dots: false,
    autoplay: false,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    nav: true,
     navText: ["<div class='btn1'> <i class='fa fa-arrow-left navleft' ></i></div>",
       "<div class='btn2'><i class='fa fa-arrow-right navright'></i></div>"],
    responsive: {
        0: {
            items: 1
        },
        735: {
            items: 2
        },
        1000: {
            items: 4
        }
    }

})

$('.Bestsellingcarousel').owlCarousel({
    loop: true,
     margin: 0, 
    dots: false,
    autoplay: false,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    nav: false,
    //  navText: ["<div class='btn1'> <i class='fa fa-arrow-left navleft' ></i></div>",
    //    "<div class='btn2'><i class='fa fa-arrow-right navright'></i></div>"],
    responsive: {
        0: {
            items: 1
        },
        735: {
            items: 2
        },
        1000: {
            items: 6
        }
    }

})


$('.container').imagesLoaded( function() {
    $("#exzoom").exzoom({
          autoPlay: true,
      });
    $("#exzoom").removeClass('hidden')

    
  });

  
  


//   var _gaq = _gaq || [];
//   _gaq.push(['_setAccount', 'UA-36251023-1']);
//   _gaq.push(['_setDomainName', 'jqueryscript.net']);
//   _gaq.push(['_trackPageview']);

//   (function() {
//     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
//     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
//     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
//   })();

$('.product_detail_carousel').owlCarousel({
    loop: true,
     margin: 0, 
    dots: false,
    autoplay: false,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    nav: false,
    //  navText: ["<div class='btn1'> <i class='fa fa-arrow-left navleft' ></i></div>",
    //    "<div class='btn2'><i class='fa fa-arrow-right navright'></i></div>"],
    responsive: {
        0: {
            items: 1
        },
        735: {
            items: 1
        },
        1000: {
            items: 1
        }
    }

})


 
   

$(window).scroll(function () {
    if ($(window).scrollTop() >= 140) {
       

       $('.navbar_search').addClass('fixed-header');
       $('.tagline_h3').addClass('Tagline_h3');
        // // $('.logo').addClass('logo_fix');
        $('.fix_logo').addClass('fixed-logo');
    }

    else {
    
      $('.navbar_search').removeClass('fixed-header');
      $('.tagline_h3').removeClass('Tagline_h3');
        // // $('.logo').removeClass('logo_fix');
        $('.fix_logo').removeClass('fixed-logo');
    }

    if ($(window).scrollTop() >= 170) {
       

        $('.navbar_search').addClass('fixed-header1');
        $('.tagline_h3').addClass('Tagline_h3');
         // // $('.logo').addClass('logo_fix');
          $('.fix_logo').addClass('fixed-logo');
     }
 
     else {
     
       $('.navbar_search').removeClass('fixed-header1');
       $('.tagline_h3').removeClass('Tagline_h3');
         // // $('.logo').removeClass('logo_fix');
         $('.fix_logo').removeClass('fixed-logo');
     }

});


// $(window).scroll(function () {
//     if ($(window).scrollTop() >= 1 && $(window).width() >= 992) {     

//          $('.triangle-down').addClass('fixed-logo');

//          $('.logo').addClass('fix-logo');


//     }


//     else {
//         $('.triangle-down').removeClass('fixed-logo');

//         $('.logo').removeClass('fix-logo');

//     }

//     if ($(window).scrollTop() >= 30 && $(window).width() >= 320 && $(window).width() <= 991 ) {     

//         $('.triangle-down').addClass('fixed-logo1');

//         $('.logo').addClass('fix-logo1');


//    }


//    else {
//        $('.triangle-down').removeClass('fixed-logo1');

//        $('.logo').removeClass('fix-logo1');

//    }

// });
// var Owl = {

//     init: function () {
//         Owl.carousel();
//     },

//     carousel: function () {
//         var owl;
//         $(document).ready(function () {

//             owl = $('.choosecarousel').owlCarousel({

//                 // stagePadding: 150,
//                 nav: false,
//                 dots: true,
//                 loop: true,
//                 autoplay: true,
//                 margin: 0, 
//                 dotsEach: 1,
//                 dotsContainer: '.test',
//                 responsive: {
//                     0: {
//                         items: 1,
                       
//                     },
//                     500: {
//                         items: 2,

//                     },
//                     800: {
//                         items: 3,

//                     },
//                     992: {
//                         item: 3
//                     },
//                     1200: {
//                         items: 3,
                       
//                     },

//                 }

//             });


//             $('.Customdots').on('click', 'li', function (e) {
//                 owl.trigger('to.owl.carousel', [$(this).index(), 500]);
//             });
//         });
//     }
// };
// $(document).ready(function () {
//     Owl.init();
// });



// $('.dishecarousel').owlCarousel({
//     loop: true,
//     // margin: 30, 
//     dots: false,
//     autoplay: true,
//     autoplayTimeout: 4000,
//     autoplayHoverPause: true,
//     nav: false,
//     responsive: {
//         0: {
//             items: 1
//         },
//         350: {
//             items: 2
//         },
//         500: {
//             items: 3
//         },
//         768: {
//             items: 4
//         },
//         992: {
//             items: 5
//         },
//         1200: {
//             items: 6
//         },

//     }

// })


// $('.dishescarousel').owlCarousel({
//     loop: true,
//     dots: true,
//     dotsEach: 3,
//     autoplay: true,
//     autoplayTimeout: 4000,
//     autoplayHoverPause: true,
//     nav: false,
//     responsive: {
//         0: {
//             items: 1

//         },
//         350: {
//             items: 2
//         },
//         500: {
//             items: 3
//         },
//         768: {
//             items: 4
//         },
//         992: {
//             items: 5
//         },
//         1200: {
//             items: 6
//         },

//     }

// })

